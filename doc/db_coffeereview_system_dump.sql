-- --------------------------------------------------------
-- Host:                         localhost
-- Server version:               5.7.24 - MySQL Community Server (GPL)
-- Server OS:                    Win64
-- HeidiSQL Version:             10.2.0.5599
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for coffeereview_db
CREATE DATABASE IF NOT EXISTS `coffeereview_db` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `coffeereview_db`;

-- Dumping structure for table coffeereview_db.coffeereview
CREATE TABLE IF NOT EXISTS `coffeereview` (
  `NumRW` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Name` varchar(50) NOT NULL DEFAULT '',
  `Surname` varchar(50) NOT NULL DEFAULT '',
  `Date` datetime NOT NULL,
  `Review` char(200) NOT NULL DEFAULT '',
  PRIMARY KEY (`NumRW`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- Dumping data for table coffeereview_db.coffeereview: ~2 rows (approximately)
/*!40000 ALTER TABLE `coffeereview` DISABLE KEYS */;
REPLACE INTO `coffeereview` (`NumRW`, `Name`, `Surname`, `Date`, `Review`) VALUES
	(1, 'Nutty', 'GGez', '2021-04-03 09:58:44', 'อยากนอนใจจะขาด ดันมามีสอบ WebPro'),
	(2, 'Chinna', 'GGez', '2021-04-03 09:58:11', 'ตังค์เหลืออยากเลี้ยงเด็ก'),
	(4, 'Lisa', 'Longsaman', '2021-04-03 09:59:22', 'รอเธอกลับมา ก็เหมือนรอรถไฟที่ร้านกาแฟ');
/*!40000 ALTER TABLE `coffeereview` ENABLE KEYS */;

-- Dumping structure for table coffeereview_db.userinformation
CREATE TABLE IF NOT EXISTS `userinformation` (
  `UserID` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(50) NOT NULL,
  `Surname` varchar(50) NOT NULL,
  `Username` varchar(50) NOT NULL,
  `Password` varchar(200) NOT NULL,
  PRIMARY KEY (`UserID`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- Dumping data for table coffeereview_db.userinformation: ~0 rows (approximately)
/*!40000 ALTER TABLE `userinformation` DISABLE KEYS */;
REPLACE INTO `userinformation` (`UserID`, `Name`, `Surname`, `Username`, `Password`) VALUES
	(1, 'Nutty', 'eiei', 'nut', 'nut'),
	(2, 'Nutty1', 'eiei1', 'nut1', '$2y$10$I41M236buf/38NfZHlhsWOVUFSbIUwHVeWXr.4csPHR2sNv8oUyRe');
/*!40000 ALTER TABLE `userinformation` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
