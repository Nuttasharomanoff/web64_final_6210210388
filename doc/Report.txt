========================================================================

Backend System: Coffee-Review -Nutty Coffee Review System-

Developed by: MR. ATHIP LONGSAMAN  StudentID 6210210388

API List 
- POST /register   //Registering new user 
Parameters : Name, Surname , username, password

- POST /login    //Login Write a review
Parameters : username, password

- GET /list review    //See the content being reviewed  //Use Token
Parameters : api_token

- POST /add_review     //Add review content
Parameters : Name, Surname , Date, Review

-PUT /update_review  // Update review content
Parameters : NumRW  ,Review

-DELETE /delete_review  // Update review content
Parameters : NumRW
======================================================================== 

========================================================================