<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/
use Firebase\JWT\JWT ;

$router->get('/login', function () use ($router) {
    return $router->app->version();
});

// แสดง list
$router->get('/list_review', ['middleware'=>'auth',function () {
	
	$results = app('db')->select("SELECT * FROM coffeereview");
	return response()->json($results);
}]);

// เพิ่มรีวิว
$router->post('/add_review', function(Illuminate\Http\Request $request) {
	
		
    $Review_name = $request->input("Review_name");	

    $Review_surname = $request->input("Review_surname");

    $Review_Date = strtotime($request->input("Review_Date"));
    $Review_Date = date('Y-m-d H:i:s', $Review_Date);
    
    $Review_Content = $request->input("Review_Content");
    
    $query = app('db')->insert('INSERT into coffeereview
                    (Name, Surname , Date, Review)
                    VALUE (?, ?, ?, ?)',
                    [ $Review_name,
                      $Review_surname,
                      $Review_Date,
                      $Review_Content ] );
    return "Ok";
});

// แก้ไขคำรีวิว
$router->put('/update_review', function(Illuminate\Http\Request $request) {
        
    $NumRW_id = $request->input("NumRW_id");
    
    $Review_Content = $request->input("Review_Content");
    
    $query = app('db')->update('UPDATE coffeereview
                                SET Review=?
                                
                                WHERE
                                    NumRW=?',
                                    [ $Review_Content ,
                                        $NumRW_id] );
        
        return "Ok";	

 });

 //ลบ คำรีวิว
 $router->delete('/delete_review', function(Illuminate\Http\Request $request) {
        
    $NumRW_id = $request->input("NumRW_id");
       
    $query = app('db')->delete('DELETE FROM coffeereview
                            
                                WHERE
                                    NumRW=?',
                                    [$NumRW_id] );
        
        return "Ok";	

 });

 //สมัครสมาชิก
 $router->post('/register', function(Illuminate\Http\Request $request) {
	

    //$User_id = $request->input("User_id");

    $name = $request->input("name");

    $surname = $request->input("surname");

    $User_name = $request->input("User_name");

    //$User_Password = $request->input("User_Password");
    $User_Password = app('hash')->make($request->input("User_Password"));
    
    $query = app('db')->insert('INSERT into userinformation
                    ( Name, Surname,Username,Password)
                    VALUE (?, ?, ?, ?)',
                    [   
                        $name,
                        $surname,
                        $User_name,
                        $User_Password,
                        ] );
    return "Ok";
    
});


//เข้าสู่ระบบ
$router->post('/login', function(Illuminate\Http\Request $request) {
	
	$User_name = $request->input("User_name");
	$User_Password = $request->input("User_Password");
	
	$result = app('db')->select("SELECT UserID,Password FROM userinformation WHERE Username=?",
									[$User_name]);
									
	$loginResult = new stdClass();
	
	if(count ($result) == 0) {
			$loginResult->status = "fail";
			$loginResult->reason = "User is not founded";
	}else {
	
		if(app('hash')->check($User_Password, $result[0]->Password)){
			$loginResult->status = "success";
            //$loginResult->reason = "Welcome";
			
			$payload = [
				'iss' => "coffeereview",
				'sub' => $result[0]->UserID,
				'iat' => time(),
				'exp' => time()+ 30 * 60 * 60,
			
			];
			
			$loginResult->token = JWT::encode($payload, env('APP_KEY'));
			//$loginResult->isAdmin = $result[0]->IsAdmin;*/
			return response()->json($loginResult);

		}else {
			$loginResult->status = "fail";
			$loginResult->reason = "Incorrect Password";

            return response()->json($loginResult);   
		}
	}
	return response()->json($loginResult);
});